﻿#include <iostream>
#include <string>

int main()
{
    std::cout << "Write your phrase and press Enter" << "\n";
    std::string Phrase;
    std::getline(std::cin, Phrase);
    std::cout << "Number of characters: " << " " << Phrase.length() << "\n";
    std::cout << "The first symbol:" << " " << Phrase.front() << "\n";
    std::cout << "The last symbol:" << " " << Phrase.back() << "\n";
}